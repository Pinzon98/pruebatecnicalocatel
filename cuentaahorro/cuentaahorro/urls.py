from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('usuario/', include('usuario.urls')),
    path('cuenta/', include('cuenta.urls')),
    path('admin/', admin.site.urls),
]
