import os
import json
import environ
import importlib
from constants import general_constants as genConst

env = environ.Env()
environ.Env.read_env()
langConst = importlib.import_module(genConst.LANGUAGE_CONSTANTS.format(env(genConst.LANGUAGE)))

def verificarCampoRequest(body, typeRequest):
    resp = {genConst.RESULT: True, genConst.ERROR: None}
    jsonTypeRequest = json.load(open(os.path.join(genConst.UTILS,genConst.TYPE_REQUEST,typeRequest+genConst.JSONFILE)))
    for campo in jsonTypeRequest.keys():
        if campo not in body.keys():
            resp[genConst.RESULT] = False
            resp[genConst.ERROR] = langConst.MSG_CAMPO_FALTANTE.format(campo)
            return resp
        if jsonTypeRequest[campo][genConst.TIPO] != str(type(body[campo])):
            resp[genConst.RESULT] = False
            resp[genConst.ERROR] = langConst.MSG_CAMPO_DIF_TIPO.format(campo, jsonTypeRequest[campo][genConst.TIPO])
            return resp
        if jsonTypeRequest[campo][genConst.POSIBLES] != None:
            if body[campo] not in jsonTypeRequest[campo][genConst.POSIBLES]:
                resp[genConst.RESULT] = False
                resp[genConst.ERROR] = langConst.MSG_CAMPO_NO_OPCION.format(campo, str(jsonTypeRequest[campo][genConst.POSIBLES]))
                return resp
    return resp