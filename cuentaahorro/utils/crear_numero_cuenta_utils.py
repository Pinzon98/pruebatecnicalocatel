from random import randint
from cuenta.models import Cuenta

def randomNumber(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    unique = False
    while not unique:
        numeroCuenta = randint(range_start, range_end)
        if not Cuenta.objects.filter(number=numeroCuenta):
            unique = True
            return numeroCuenta