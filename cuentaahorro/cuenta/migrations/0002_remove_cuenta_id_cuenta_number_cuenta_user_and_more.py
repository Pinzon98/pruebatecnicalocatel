# Generated by Django 4.2.4 on 2023-08-07 04:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0001_initial'),
        ('cuenta', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cuenta',
            name='id',
        ),
        migrations.AddField(
            model_name='cuenta',
            name='number',
            field=models.BigIntegerField(default=100000000, primary_key=True, serialize=False),
        ),
        migrations.AddField(
            model_name='cuenta',
            name='user',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='usuario.usuario'),
        ),
        migrations.AddField(
            model_name='cuenta',
            name='valor',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=50),
        ),
    ]
