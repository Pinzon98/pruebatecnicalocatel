from django.db import models
from usuario.models import Usuario

class Cuenta(models.Model):
    number = models.BigIntegerField(primary_key=True, default=100000000)
    valor = models.DecimalField(decimal_places=2, max_digits=50)
    user = models.ForeignKey(Usuario, on_delete=models.CASCADE)