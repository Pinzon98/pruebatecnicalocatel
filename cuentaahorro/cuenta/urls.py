from django.urls import path
from . import views
from .constants import url_constants as urlConst

urlpatterns = [
    path(urlConst.CONSIGNAR, views.consignar, name= urlConst.CONSIGNAR),
    path(urlConst.RETIRAR, views.retirar, name= urlConst.RETIRAR)
]