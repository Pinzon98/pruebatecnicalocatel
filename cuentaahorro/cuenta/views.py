import json
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from constants import general_constants as genConst
from cuenta.models import Cuenta
from utils.verification_request_utils import verificarCampoRequest

@csrf_exempt
def consignar(request):
    # Se convierte el request en json
    body = json.loads(request.body)
    body[genConst.VALOR] = float(body[genConst.VALOR])
    # Se verifican los campos del request obtenido
    verificacionCampReq = verificarCampoRequest(body, genConst.ACTUALIZAR_VALOR_CUENTA)
    if not verificacionCampReq[genConst.RESULT]:
        return JsonResponse(verificacionCampReq)
    # Se consigna a la cuenta
    cuenta = Cuenta.objects.get(number=body[genConst.NUMERO_CUENTA])
    cuenta.valor = float(cuenta.valor) + body[genConst.VALOR]
    cuenta.save()
    return HttpResponse(200)


@csrf_exempt
def retirar(request):
    # Se convierte el request en json
    body = json.loads(request.body)
    body[genConst.VALOR] = float(body[genConst.VALOR])
    # Se verifican los campos del request obtenido
    verificacionCampReq = verificarCampoRequest(body, genConst.ACTUALIZAR_VALOR_CUENTA)
    if not verificacionCampReq[genConst.RESULT]:
        return JsonResponse(verificacionCampReq)
    # Se retira de la cuenta
    cuenta = Cuenta.objects.get(number=body[genConst.NUMERO_CUENTA])
    cuenta.valor = float(cuenta.valor) - body[genConst.VALOR]
    cuenta.save()
    return HttpResponse(200)