from django.db import models

class Usuario(models.Model):
    username = models.CharField(max_length=10, primary_key=True)
    password = models.CharField(max_length=30)
    email = models.EmailField(max_length=100, unique=True)
    name = models.CharField(max_length=80)