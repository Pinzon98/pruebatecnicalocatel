import json
import environ
import importlib
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Usuario
from .constants import usuario_constants as usuConst
from constants import general_constants as genConst
from cuenta.models import Cuenta
from cuenta.constants import cuenta_constants as cuenConst
from utils.crear_numero_cuenta_utils import randomNumber
from utils.verification_request_utils import verificarCampoRequest

env = environ.Env()
environ.Env.read_env()
langConst = importlib.import_module(genConst.LANGUAGE_CONSTANTS.format(env(genConst.LANGUAGE)))

@csrf_exempt
def createUser(request):
    resp = {genConst.RESULT: True, genConst.RESPONSE: None, genConst.ERROR: None}
    # Se convierte el request en json
    body = json.loads(request.body)
    body[genConst.VALOR] = float(body[genConst.VALOR])
    # Se verifican los campos del request obtenido
    verificacionCampReq = verificarCampoRequest(body, genConst.CREATE_USER)
    if not verificacionCampReq[genConst.RESULT]:
        return JsonResponse(verificacionCampReq)
    # Se crea el usuario
    usuario = Usuario.objects.filter(username = body[usuConst.USERNAME])
    if len(usuario) == 0:
        usuario = Usuario(
                username = body[usuConst.USERNAME],
                password = body[usuConst.PASSWORD],
                email = body[usuConst.EMAIL],
                name = body[usuConst.NAME]
            )
        cuenta = Cuenta(
            valor = body[cuenConst.VALUE],
            user = usuario,
            number = randomNumber(9)
        )
        if request.method == genConst.POST:
            usuario.save()
            cuenta.save()
    else:
        resp[genConst.RESULT] = False
        resp[genConst.ERROR] = langConst.MSG_USUARIO_EXISTE
    return JsonResponse(resp)

def consultarUsuario(request, username, password):
    resp = {genConst.RESULT: True, genConst.RESPONSE: None, genConst.ERROR: None}
    usuario = Usuario.objects.filter(username = username)
    if len(usuario) > 0:
        if usuario.values()[0][genConst.PASSWD] == password:
            cuentas = Cuenta.objects.filter(user = Usuario.objects.get(username = username))
            listaCuentas = []
            for cuenta in cuentas.values():
                listaCuentas.append(cuenta)
            resp[genConst.RESPONSE] = listaCuentas
        else:
            resp[genConst.ERROR] = langConst.MSG_CLAVE_NO_COINCIDE
            resp[genConst.RESULT] = False
    else:
        resp[genConst.ERROR] = langConst.MSG_NO_EXISTE_USUARIO
        resp[genConst.RESULT] = False
    return JsonResponse(resp)