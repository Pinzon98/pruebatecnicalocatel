from django.urls import path
from . import views
from .constants import url_constants as urlConst

urlpatterns = [
    path(urlConst.CREATE_USER, views.createUser, name= urlConst.CREATE_USER),
    path(urlConst.CONSULTAR_USUARIO, views.consultarUsuario, name= urlConst.CONSULTAR_USUARIO)
]